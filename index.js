// console.log('Hello AL');

/*JSON - javascript object notation -- it is a popular data format which is application is communicate with each other

JSON - apply named after JS objects looklike JS Object with{ nad key-value pairs. The key of JSON obeject si sorrounded by "". 

	{
	"key1":"ValueA",
	"key2": 25000
	}
	*/
let sampleJSON = `
{
	"name": "Katniss Everdeen",
	"age": 20,
	"address" : {
		"city": "Kansas city",
		"state": "Kansas"
	}
}
`;

console.log(sampleJSON);
console.log(typeof sampleJSON);

// Are we able to turn JSON into a JS Object?
// JSON.parse() - will return the given JSON as a JS object.
// JSON -> JS Object - JSON.parse()
let sampleObject1 = JSON.parse(sampleJSON);
console.log(sampleObject1);
console.log(typeof sampleObject1);

// JSON.stringify() - will return a given JS object as a stringfied JSON format string
// JS Object -> JSON - JSON.stringify()
let user1 = {
	username: 'knight123',
	password: '1234',
	age: 25
}

let sampleStringfied = JSON.stringify(user1);
console.log(sampleStringfied);
console.log(typeof sampleStringfied);

// JSON array
// Array of JSON in JSON format

let sampleArr = `
	[
		{
			"email": "jasonDerulo@gmail.com",
			"password": "safe1234",
			"isAdmin": false
		},

		{
			"email": "jasonDerulo@gmail.com",
			"password": "jasyon086",
			"isAdmin": false
		}
	]
`;

console.log(sampleArr);
console.log(typeof sampleArr);
// How can we delete the last JSON array?

// First turn the JSON format array to aJS array
let parsedArr = JSON.parse(sampleArr);
console.log(parsedArr);
console.log(typeof sampleArr);

// delete the last item in array using pop()
parsedArr.pop();
console.log(parsedArr);

// We can now turn parse back to into JSON and update the sampleArr JSO array
sampleArr = JSON.stringify(parsedArr);
console.log(sampleArr);

// DO's and Dont's


// DO
// 	Do: add souble woutes to your keys
// 	DO: add colon after each key

// DONT'S
//  Don't add  excessive commas
//  Don't forget to close double qoutes, curly brace


let sampleData = `
{
	"email": "jasonDerulo@gmail.com",
	"password": "1234james",
	"balance": 50000,
	"isAdmin": false
}`;
// WHen you parse JSON with an erratic format, it produces an error.
let parseData = JSON.parse(sampleData);
console.log(parseData);

// Sublime Text Tip: Tcheck the format of JSON by change the linting from JS to JSON (lower left of sublime)
